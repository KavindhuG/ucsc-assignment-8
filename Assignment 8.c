#include <stdio.h>

struct student
{
  char fname[30];
  char sname[30];
  char subject[30];
  int marks;
};
int main()
{
  int i,total;
  printf("Enter the number of students: \n");
  scanf("%d",&total);
  struct student stud[total];
  
  for(i=0; i<total; i++)
    {
      printf("Student %d\n",i+1);
      printf("Enter student's first name :\n");
      scanf("%s", stud[i].fname);
      printf("Enter students surname :\n");
      scanf("%s", stud[i].sname);
      printf("Enter subject :\n");
      scanf("%s",stud[i].subject);
      printf("Enter marks :\n");
      scanf("%d", &stud[i].marks);
      
    }
  printf("Displaying the gathered data of students... \n");
  for(i=0; i<total; i++)
    {
      printf("Student %d\n",i+1);
      printf("First Name : %s\n", stud[i].fname);
      printf("Surname : %s\n", stud[i].sname);
      printf("Subject : %s\n", stud[i].subject);
      printf("Marks : %d\n", stud[i].marks);
    }
  return 0;
}